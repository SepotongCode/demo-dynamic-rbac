package com.demo.dynamic.rbac.service.impl;

import com.demo.dynamic.rbac.dto.request.TransactionDetailRequest;
import com.demo.dynamic.rbac.dto.response.TransactionDetailResponse;
import com.demo.dynamic.rbac.entity.Transaction;
import com.demo.dynamic.rbac.repository.TransactionRepository;
import com.demo.dynamic.rbac.service.contract.TransactionDetailService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TransactionDetailServiceImpl implements TransactionDetailService {

    private final TransactionRepository transactionRepository;

    @Override
    public TransactionDetailResponse execute(TransactionDetailRequest request) {
        Optional<Transaction> checkTransaction = transactionRepository.findById(request.getId());
        if (!checkTransaction.isPresent()) {
            throw new EntityNotFoundException();
        }
        Transaction transaction = checkTransaction.get();

        return buildResponse(transaction);
    }

    private TransactionDetailResponse buildResponse(Transaction transaction) {
        TransactionDetailResponse response = new TransactionDetailResponse();
        BeanUtils.copyProperties(transaction,response);
        return response;
    }
}
