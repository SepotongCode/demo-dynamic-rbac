package com.demo.dynamic.rbac.service.impl;

import com.demo.dynamic.rbac.dto.criteria.TransactionListCriteria;
import com.demo.dynamic.rbac.dto.response.TransactionDetailResponse;
import com.demo.dynamic.rbac.entity.Transaction;
import com.demo.dynamic.rbac.repository.TransactionRepository;
import com.demo.dynamic.rbac.service.contract.TransactionListService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TransactionListServiceImpl implements TransactionListService {

    private final TransactionRepository transactionRepository;

    @Override
    public List<TransactionDetailResponse> execute(TransactionListCriteria request) {
        List<Transaction> transactions = transactionRepository.findAll();

        return buildResponse(transactions);
    }

    private List<TransactionDetailResponse> buildResponse(List<Transaction> transactions) {

        return transactions.stream()
                .map(transaction -> {

                    TransactionDetailResponse response = new TransactionDetailResponse();
                    BeanUtils.copyProperties(transaction,response);
                    return response;
                }).toList();
    }
}
