package com.demo.dynamic.rbac.service.contract;

import com.demo.dynamic.rbac.base.command.Command;
import com.demo.dynamic.rbac.dto.request.TransactionDetailRequest;
import com.demo.dynamic.rbac.dto.response.TransactionDetailResponse;

public interface TransactionDetailService extends Command<TransactionDetailRequest, TransactionDetailResponse> {
}
