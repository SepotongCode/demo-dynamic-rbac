package com.demo.dynamic.rbac.service.impl;

import com.demo.dynamic.rbac.entity.AppUser;
import com.demo.dynamic.rbac.entity.AppUserRole;
import com.demo.dynamic.rbac.entity.Role;
import com.demo.dynamic.rbac.entity.RoleAccess;
import com.demo.dynamic.rbac.repository.AppUserRepository;
import com.demo.dynamic.rbac.repository.AppUserRoleRepository;
import com.demo.dynamic.rbac.repository.RoleAccessRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Service("authUserDetailsService")
@RequiredArgsConstructor
public class AuthUserDetailsService implements UserDetailsService {

    private final AppUserRepository appUserRepository;

    private final AppUserRoleRepository appUserRoleRepository;

    private final RoleAccessRepository roleAccessRepository;

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.debug("load user by username: {}", username);
        Optional<AppUser> checkUser = appUserRepository.findFirstByUsername(username);
        if(!checkUser.isPresent()){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        AppUser user = checkUser.get();
        return new User(user.getUsername(), user.getPassword(), getAuthority(user));
    }

    private Set<SimpleGrantedAuthority> getAuthority(AppUser user) {
        log.debug("load user role by user for user detail");
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        List<AppUserRole> userRoleList = appUserRoleRepository.findByAppUserId(user.getId());
        userRoleList.forEach(userRole -> {
            String role = "ROLE_".concat(userRole.getRole().getCode());
            log.debug("add role for otorisasi");
            authorities.add(new SimpleGrantedAuthority(role));
            log.debug("add access for otorisasi");
            getAuthorityAccess(authorities,userRole.getRole());
        });
        return authorities;
    }

    private void getAuthorityAccess(Set<SimpleGrantedAuthority> authorities, Role role) {
        log.debug("load role access by role for user detail");
        List<RoleAccess> roleAccesses = roleAccessRepository.findByRoleId(role.getId());
        for (RoleAccess access : roleAccesses) {
            String roleAccess = "ROLE_".concat(access.getAccess().getCode());
            authorities.add(new SimpleGrantedAuthority(roleAccess));
        }
    }
}