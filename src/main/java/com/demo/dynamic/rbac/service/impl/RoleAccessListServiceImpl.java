package com.demo.dynamic.rbac.service.impl;

import com.demo.dynamic.rbac.dto.criteria.RoleAccessListCriteria;
import com.demo.dynamic.rbac.dto.criteria.TransactionListCriteria;
import com.demo.dynamic.rbac.dto.response.RoleAccessListResponse;
import com.demo.dynamic.rbac.dto.response.TransactionDetailResponse;
import com.demo.dynamic.rbac.entity.RoleAccess;
import com.demo.dynamic.rbac.entity.Transaction;
import com.demo.dynamic.rbac.repository.RoleAccessRepository;
import com.demo.dynamic.rbac.repository.TransactionRepository;
import com.demo.dynamic.rbac.service.contract.RoleAccessListService;
import com.demo.dynamic.rbac.service.contract.TransactionListService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RoleAccessListServiceImpl implements RoleAccessListService {

    private final RoleAccessRepository roleAccessRepository;

    @Override
    public List<RoleAccessListResponse> execute(RoleAccessListCriteria request) {
        List<RoleAccess> roleAccesses = roleAccessRepository.findAll();

        return buildResponse(roleAccesses);
    }

    private List<RoleAccessListResponse> buildResponse(List<RoleAccess> roleAccesses) {

        Set<String> accesses = new HashSet<>();

        for (RoleAccess roleAccess : roleAccesses) {
            accesses.add(roleAccess.getRole().getCode());
            accesses.add(roleAccess.getAccess().getCode());
        }

        return roleAccesses.stream()
                .map(roleAccess -> RoleAccessListResponse.builder()
                        .roleCode(roleAccess.getRole().getCode())
                        .accessCode(roleAccess.getAccess().getCode())
                        .accessUri(roleAccess.getAccess().getUri())
                        .build())
                .toList();

    }
}
