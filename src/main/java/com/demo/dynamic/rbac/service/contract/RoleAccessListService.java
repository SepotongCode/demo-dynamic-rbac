package com.demo.dynamic.rbac.service.contract;

import com.demo.dynamic.rbac.base.command.Command;
import com.demo.dynamic.rbac.dto.criteria.RoleAccessListCriteria;
import com.demo.dynamic.rbac.dto.criteria.TransactionListCriteria;
import com.demo.dynamic.rbac.dto.response.RoleAccessListResponse;
import com.demo.dynamic.rbac.dto.response.TransactionDetailResponse;

import java.util.List;
import java.util.Set;

public interface RoleAccessListService extends Command<RoleAccessListCriteria, List<RoleAccessListResponse>> {
}
