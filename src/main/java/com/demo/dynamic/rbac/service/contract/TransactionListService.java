package com.demo.dynamic.rbac.service.contract;

import com.demo.dynamic.rbac.base.command.Command;
import com.demo.dynamic.rbac.dto.criteria.TransactionListCriteria;
import com.demo.dynamic.rbac.dto.response.TransactionDetailResponse;

import java.util.List;

public interface TransactionListService extends Command<TransactionListCriteria, List<TransactionDetailResponse>> {
}
