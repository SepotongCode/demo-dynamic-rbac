package com.demo.dynamic.rbac.base.command;


import com.demo.dynamic.rbac.base.dto.request.ServiceRequest;

public interface ServiceExecutor {

    <REQUEST extends ServiceRequest, RESPONSE> RESPONSE execute(Class<? extends Command<REQUEST,RESPONSE>> commandClass, REQUEST request);

}
