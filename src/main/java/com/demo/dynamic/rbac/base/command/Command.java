package com.demo.dynamic.rbac.base.command;

import com.demo.dynamic.rbac.base.dto.request.ServiceRequest;

public interface Command<REQUEST extends ServiceRequest, RESPONSE>{

    RESPONSE execute(REQUEST request);

}
