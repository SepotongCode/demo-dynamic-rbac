package com.demo.dynamic.rbac.dto.request;

import com.demo.dynamic.rbac.base.dto.request.ServiceRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class TransactionDetailRequest implements ServiceRequest {

    private Long id;

}
