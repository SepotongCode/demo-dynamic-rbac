package com.demo.dynamic.rbac.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class TransactionDetailResponse {

    private Long id;

    private String invoice;

    private BigDecimal amount;

    private LocalDateTime createDate;

    private LocalDateTime updateDate;

}
