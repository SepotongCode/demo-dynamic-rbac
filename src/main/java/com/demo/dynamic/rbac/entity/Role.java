package com.demo.dynamic.rbac.entity;

import com.demo.dynamic.rbac.base.entity.BaseIdEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "role")
public class Role extends BaseIdEntity {

    @Column(name = "code")
    private String code;

    @Column(name = "title")
    private String title;

}
