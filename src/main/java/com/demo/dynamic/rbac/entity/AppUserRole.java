package com.demo.dynamic.rbac.entity;

import com.demo.dynamic.rbac.base.entity.BaseIdEntity;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "app_user_role")
public class AppUserRole extends BaseIdEntity {

    @ManyToOne
    @JoinColumn(name = "app_user_id")
    private AppUser appUser;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

}
