package com.demo.dynamic.rbac.entity;

import com.demo.dynamic.rbac.base.entity.BaseIdEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Entity
@Table(name = "transaction")
public class Transaction extends BaseIdEntity {

    @Column(name = "invoice")
    private String invoice;

    @Column(name = "amount")
    private BigDecimal amount;

}
