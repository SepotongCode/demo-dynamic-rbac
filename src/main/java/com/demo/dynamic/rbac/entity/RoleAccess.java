package com.demo.dynamic.rbac.entity;

import com.demo.dynamic.rbac.base.entity.BaseIdEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "role_access")
public class RoleAccess extends BaseIdEntity {

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

    @ManyToOne
    @JoinColumn(name = "access_id")
    private Access access;

}
