package com.demo.dynamic.rbac.entity;

import com.demo.dynamic.rbac.base.entity.BaseIdEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "app_user")
public class AppUser extends BaseIdEntity {

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

}
