package com.demo.dynamic.rbac.entity;

import com.demo.dynamic.rbac.base.entity.BaseIdEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "access")
public class Access extends BaseIdEntity {

    @Column(name = "code")
    private String code;

    @Column(name = "uri")
    private String uri;

}
