package com.demo.dynamic.rbac.repository;

import com.demo.dynamic.rbac.base.repository.BaseJpaPagingRepository;
import com.demo.dynamic.rbac.entity.AppUserRole;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AppUserRoleRepository extends BaseJpaPagingRepository<AppUserRole,Long> {
    List<AppUserRole> findByAppUserId(Long id);
}
