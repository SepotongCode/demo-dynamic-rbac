package com.demo.dynamic.rbac.repository;

import com.demo.dynamic.rbac.base.repository.BaseJpaPagingRepository;
import com.demo.dynamic.rbac.entity.AppUser;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AppUserRepository extends BaseJpaPagingRepository<AppUser,Long> {
    Optional<AppUser> findFirstByUsername(String username);
}
