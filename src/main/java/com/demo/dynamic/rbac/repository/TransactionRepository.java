package com.demo.dynamic.rbac.repository;

import com.demo.dynamic.rbac.base.repository.BaseJpaPagingRepository;
import com.demo.dynamic.rbac.entity.Transaction;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends BaseJpaPagingRepository<Transaction,Long> {
}
