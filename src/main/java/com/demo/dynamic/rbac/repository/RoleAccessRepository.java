package com.demo.dynamic.rbac.repository;

import com.demo.dynamic.rbac.base.repository.BaseJpaPagingRepository;
import com.demo.dynamic.rbac.entity.RoleAccess;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleAccessRepository extends BaseJpaPagingRepository<RoleAccess,Long> {
    List<RoleAccess> findByRoleId(Long id);
}
