package com.demo.dynamic.rbac.repository;

import com.demo.dynamic.rbac.base.repository.BaseJpaPagingRepository;
import com.demo.dynamic.rbac.entity.Role;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends BaseJpaPagingRepository<Role,Long> {
}
