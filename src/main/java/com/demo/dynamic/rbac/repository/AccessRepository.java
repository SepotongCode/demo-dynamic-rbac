package com.demo.dynamic.rbac.repository;

import com.demo.dynamic.rbac.base.repository.BaseJpaPagingRepository;
import com.demo.dynamic.rbac.entity.Access;
import org.springframework.stereotype.Repository;

@Repository
public interface AccessRepository extends BaseJpaPagingRepository<Access,Long> {
}
