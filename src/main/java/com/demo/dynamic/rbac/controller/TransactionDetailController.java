package com.demo.dynamic.rbac.controller;

import com.demo.dynamic.rbac.base.command.ServiceExecutor;
import com.demo.dynamic.rbac.dto.request.TransactionDetailRequest;
import com.demo.dynamic.rbac.service.contract.TransactionDetailService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("transactions")
@RequiredArgsConstructor
public class TransactionDetailController {

    private final ServiceExecutor serviceExecutor;

    @GetMapping("/detail/{id}")
    public ResponseEntity doExecute(@PathVariable("id") Long id) {

        var response = serviceExecutor.execute(TransactionDetailService.class, TransactionDetailRequest.builder()
                        .id(id)
                .build());

        return ResponseEntity.ok(response);
    }

}
