package com.demo.dynamic.rbac.controller;

import com.demo.dynamic.rbac.base.command.ServiceExecutor;
import com.demo.dynamic.rbac.dto.criteria.TransactionListCriteria;
import com.demo.dynamic.rbac.dto.request.TransactionDetailRequest;
import com.demo.dynamic.rbac.service.contract.TransactionDetailService;
import com.demo.dynamic.rbac.service.contract.TransactionListService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("transactions")
@RequiredArgsConstructor
public class TransactionListController {

    private final ServiceExecutor serviceExecutor;

    @GetMapping("/list")
    public ResponseEntity doExecute() {

        var responses = serviceExecutor.execute(TransactionListService.class, new TransactionListCriteria());

        return ResponseEntity.ok(responses);
    }

}
