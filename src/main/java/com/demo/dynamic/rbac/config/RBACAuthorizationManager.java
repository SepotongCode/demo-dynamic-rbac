package com.demo.dynamic.rbac.config;

import com.demo.dynamic.rbac.base.command.ServiceExecutor;
import com.demo.dynamic.rbac.dto.criteria.RoleAccessListCriteria;
import com.demo.dynamic.rbac.dto.response.RoleAccessListResponse;
import com.demo.dynamic.rbac.service.contract.RoleAccessListService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.authorization.AuthorityAuthorizationManager;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.authorization.AuthorizationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.access.intercept.RequestAuthorizationContext;
import org.springframework.security.web.access.intercept.RequestMatcherDelegatingAuthorizationManager;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;

import java.util.function.Supplier;

@Slf4j
@RequiredArgsConstructor
@Component
public class RBACAuthorizationManager implements AuthorizationManager<RequestAuthorizationContext> {

    private RequestMatcherDelegatingAuthorizationManager delegate;

    private final ServiceExecutor serviceExecutor;

    @Override
    public AuthorizationDecision check(Supplier<Authentication> authentication, RequestAuthorizationContext object) {
        return this.delegate.check(authentication, object.getRequest());
    }

    @Override
    public void verify(Supplier<Authentication> authentication, RequestAuthorizationContext object) {
        AuthorizationManager.super.verify(authentication, object);
    }

    @EventListener
    void applyRules(ApplicationReadyEvent event) {
        RequestMatcherDelegatingAuthorizationManager.Builder builder = RequestMatcherDelegatingAuthorizationManager.builder();

        String defaultSaRole = "SA";

        try {
            var accesses = serviceExecutor.execute(RoleAccessListService.class, new RoleAccessListCriteria());
            for (RoleAccessListResponse access : accesses) {
                log.debug("applyRules: code {}, uri {}", access.getAccessCode(), access.getAccessUri());
                if (access.getAccessUri() != null && !access.getAccessUri().isBlank()) {
                    builder.add(new AntPathRequestMatcher(access.getAccessUri()),
                            AuthorityAuthorizationManager
                                    .hasAnyAuthority("ROLE_" + defaultSaRole, "ROLE_" + access.getAccessCode())
                    );
                }
            }
            builder.add(new AntPathRequestMatcher("/**"), AuthorityAuthorizationManager.hasAuthority("ROLE_" + defaultSaRole.toUpperCase()));

            this.delegate = builder.build();
        } catch (Exception ex) {
            log.error("failed load task: {}", ex.getMessage());
            log.error("failed load task: {}", ex);
        }

    }

}