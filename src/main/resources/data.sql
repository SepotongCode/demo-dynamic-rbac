--- app_user data pass test123
insert into app_user(username,password,create_date,update_date) values ('admin','$2a$10$wEqPPmACWUIlGxsztPv1TuGWFmEmGNscgLglsH5jz.o6AkiBu/6EG',now(),now());
insert into app_user(username,password,create_date,update_date) values ('member','$2a$10$wEqPPmACWUIlGxsztPv1TuGWFmEmGNscgLglsH5jz.o6AkiBu/6EG',now(),now());

--- role data
insert into role(code,title,create_date,update_date) values ('ADMIN','Admin',now(),now());
insert into role(code,title,create_date,update_date) values ('MEMBER','Member',now(),now());

--- access data
insert into access(code,uri,create_date,update_date) values ('TRANSACTION_DETAIL','/transactions/detail/**',now(),now());
insert into access(code,uri,create_date,update_date) values ('TRANSACTION_LIST','/transactions/list',now(),now());

--- app_user_role data
insert into app_user_role(app_user_id,role_id,create_date,update_date)
values
    ((select id from app_user where username='admin' limit 1),
    (select id from role where code='ADMIN' limit 1),now(),now());
insert into app_user_role(app_user_id,role_id,create_date,update_date)
values
    ((select id from app_user where username='member' limit 1),
    (select id from role where code='MEMBER' limit 1),now(),now());

--- role_access data
---- admin RBAC data
insert into role_access(role_id,access_id,create_date,update_date)
values
    ((select id from role where code='ADMIN' limit 1),
    (select id from access where code='TRANSACTION_DETAIL' limit 1),now(),now());
insert into role_access(role_id,access_id,create_date,update_date)
values
    ((select id from role where code='ADMIN' limit 1),
    (select id from access where code='TRANSACTION_LIST' limit 1),now(),now());
---- member RBAC data
insert into role_access(role_id,access_id,create_date,update_date)
values
    ((select id from role where code='MEMBER' limit 1),
    (select id from access where code='TRANSACTION_LIST' limit 1),now(),now());

--- transaction data
insert into transaction(invoice,amount,create_date,update_date) values ('inv-1',10000,now(),now());
insert into transaction(invoice,amount,create_date,update_date) values ('inv-2',20000,now(),now());
insert into transaction(invoice,amount,create_date,update_date) values ('inv-3',30000,now(),now());
insert into transaction(invoice,amount,create_date,update_date) values ('inv-4',40000,now(),now());
insert into transaction(invoice,amount,create_date,update_date) values ('inv-5',50000,now(),now());
insert into transaction(invoice,amount,create_date,update_date) values ('inv-6',60000,now(),now());
